<?php
    include 'db-connection';
    if(isset($_POST['btnLogin'])){
        $defaultUser = "dara";
        $defaultPassword = "123";
        $username = $_POST['username'];
        $password = $_POST['password'];

        if($username == $defaultUser && $password == $defaultPassword){
            $_SESSION['loggedIn'] = "SUCCESS";
            header("location: productlist.php"); 
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Homepage</title>
</head>
<body>
<div class="container">
    <form action="" method="POST" role="form">
        <legend>Form title</legend>
    
        <div class="form-group">
            <label for="text">Username</label>
            <input type="text" class="form-control" name="username" placeholder="Input field">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Input field">
        </div>
    
        <button type="submit" class="btn btn-primary" name="btnLogin">Log In</button>
    </form>
</div>

</body>
</html>