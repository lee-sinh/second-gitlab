<?php
include '../db-connection.php';

$search = isset($_GET['search']) ? $_GET['search'] : '';

$sql = "SELECT p.id, p.price, p.name, c.name AS category, p.description, i.picture_url
        FROM product AS p
        JOIN category AS c ON p.category_id = c.id
        LEFT JOIN image AS i ON p.image_id = i.id
        WHERE p.name LIKE '%$search%' OR c.name LIKE '%$search%'";

$result = $con->query($sql);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Results</title>
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h2>Search Results</h2>
    
    <p>Search results for: <?php echo htmlspecialchars($search); ?></p>
    
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    $id = $row['id'];
                    $name = $row['name'];
                    $price = $row['price'];
                    $category = $row['category'];
                    $description = $row['description'];
                    $image = $row['picture_url'];

                    echo "<tr>
                        <td>$id</td>
                        <td>$name</td>
                        <td>$price$</td>
                        <td>$category</td>
                        <td>$description</td>
                        <td><img src='$image' height='40px'></td>
                        </tr>";
                }
            } else {
                echo "<tr><td colspan='6'>No results found.</td></tr>";
            }
            ?>
        </tbody>
    </table>
</div> 
</body>
</html>
