<?php
    include '../db-connection.php';
    if(isset($_POST['btnLogin'])){
        $curr_username = $_POST['name'];
        $curr_password = $_POST['psw'];

        $query =  "SELECT * FROM admin WHERE username=? AND password=?";
        $prep = $con->prepare($query);
        $prep->bind_param("ss", $curr_username, $curr_password);
        $prep->execute();
        $result = $prep->get_result();

        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                session_start();
                $_SESSION["username"]=$curr_username;
                $_SESSION["password"]=$curr_password;

                if($curr_username == $_SESSION["username"] && $curr_password == $_SESSION["password"]){
                    $_SESSION['loggedIn'] = "SUCCESS";
                    header("location: admin_page.php"); 
                }
            }
        } else{
            echo "Invalid";
        }
        $prep->close();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>LOGIN</title>
</head>
<body>
<div class="container">
    <form action="" method="POST" role="form">
        <legend><strong>Login for Admin</strong></legend>

        <div class="form-group">
            <label for="text">Username</label>
            <input type="text" class="form-control" name="name" placeholder="Enter Username">
            <div id="" class="form-text">
                No space!
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="psw" placeholder="Enter Password">
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="btnLogin">Login</button>
    </form>
    <br>
    <a href="register.php">Create new account</a>
</div>

</body>
</html>