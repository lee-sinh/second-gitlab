<?php
    include '../db-connection.php';
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        if(isset($_POST['btnAdd'])){

                $firstname = $_POST['fname'];
                $lastname = $_POST['lname'];
                $tel = $_POST['phone'];
                $email = $_POST['email'];
                $avatar = $_POST['avatar'];
                $username = $_POST['username'];
                $password = md5($_POST['password']);
            
                $query = "INSERT INTO user (first_name, last_name, phone, email, avatar, username, password) 
                VALUES(?, ?, ?, ?, ?, ?, ?)";  //This called prepare statment (use it to prevent SQL injection)
                $result = $con->prepare($query);
                $result->bind_param("sssssss", $firstname, $lastname, $tel, $email, $avatar, $username, $password);
                if($result->execute()){
                    echo "Register Successfully";
                } else{
                    echo "Error Registering" . $result->error;
                }
                $result->close();


                if($_SESSION['registered'] = "SUCCESS"){
                    header("location: listproduct.php");
                }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Register</title>
</head>
<body>
<div class="container">
    <form action="" method="POST" role="form">
        <legend><strong>Register to continue</strong></legend>
        <div class="form-group">
            <label for="text">First Name</label>
            <input type="text" class="form-control" name="fname" placeholder="Enter first name">
        </div>
        <br>
        <div class="form-group">
            <label for="text">Last Name</label>
            <input type="text" class="form-control" name="lname" placeholder="Enter last name">
        </div>
        <br>
        <div class="form-group">
            <label for="text">Phone Number</label>
            <input type="text" class="form-control" name="phone" placeholder="Enter phone number">
        </div>
        <br>
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Email</label>
            <input type="email" class="form-control" name="email" placeholder="name@example.com">
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Upload your Avatar</label>
            <input class="form-control" type="file" id="formFile" name="avatar">
        </div>

        <div class="form-group">
            <label for="text">Username</label>
            <input type="text" class="form-control" name="username" placeholder="Enter Username">
            <div id="" class="form-text">
                No space!
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Enter Password">
        </div>
        <br>
        <button type="submit" class="btn btn-primary" name="btnAdd">Add</button>
    </form>
</div>

</body>
</html>
