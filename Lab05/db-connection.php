<?php
    $server = "localhost";
    $username = "root";
    $password = "root";
    $database = "ecommerce_db";

    $con = new mysqli($server, $username, $password, $database, 8889);

    if($con->connect_error){
        echo "<h3>Error database connection!</h3>";
    }
    else{
        echo "<h3>Connected to database successfully!</h3>";
    }

?>