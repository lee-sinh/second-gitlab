<?php
    session_start();
    echo "<script> alert('Verifying login')</script>";
    if(!isset($_SESSION['loggedIn'])){
        header("location: login.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>

<div class="container">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'db-connection.php';

                // $sql =  'SELECT * FROM product';

                // $data = $con->query($sql);

                $result = $con->query("SELECT * FROM product");

                //var_dump($data)
                if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        $name = $row['name'];
                        $price = $row['price'];
                        $description = $row['description'];

                    echo "<tr>
                        <td>$name</td>
                        <td>$price$</td>
                        <td>$description</td>
                        </tr>";
                    }
                }
            ?>
        </tbody>
    </table>
</div>

    
</body>
</html>