<?php
  //session_start(); //use this function to avoid overwritten products.
  if(!isset($_SESSION['products'])){
    $_SESSION['products'] = [
        ["product_ID" => "01", 
        "name" => "Clear",
        "description" => "perfect",
        "category" => "shampoo",
        "image" => "https://assets.unileversolutions.com/v1/1799341.png"],
    ];
  }

  if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(isset($_POST["submit"])){
      $id = $_POST["id"];
      $name = $_POST["name"];
      $description = $_POST["des"];
      $category = $_POST["cate"];
      //$image = $_POST["img"];

      $files = $_FILES['image'];
      var_dump($files);
      $i = 0;

      foreach ($files['name'] as $name) {
        $tmp_name = $files['tmp_name'][$i];

        move_uploaded_file($tmp_name, 'imageUploaded/'.$name);
        //echo "<img src='imageUploaded/$name' height=40>";
        $i++;
      }

      // Add the $newProduct to the $products array
      $newProduct = [
        "product_ID" => $id,
        "name" => $name,
        "description" => $description,
        "category" => $category,
        "image" => $name
      ];

      $_SESSION['products'][] = $newProduct;

    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="container mt-3">
        <h2>All Products</h2>         
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Category</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($_SESSION['products'] as $product){
              echo "<tr>";
                  echo "<td>".$product['product_ID']."</td>";
                  echo "<td>".$product['name']."</td>";
                  echo "<td>".$product['description']."</td>";
                  echo "<td>".$product['category']."</td>";
                  echo "<td><img src=' ".$product["image"] . " ' height='40px'></td>";
                  //echo "<td><img src='imageUploaded/" . $product["image"] . "' height='40px'></td>";

              echo "</tr>";
            }            
            ?>

          </tbody>
      </table>

      <h2>Add new product</h2>
      <form action="" method="POST">
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">Product ID</label>
          <input type="text" name="id" class="form-control" id="exampleFormControlInput1" placeholder="">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">Name</label>
          <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">Description</label>
          <input type="text" name="des" class="form-control" id="exampleFormControlInput1" placeholder="">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlInput1" class="form-label">Category</label>
          <input type="text" name="cate" class="form-control" id="exampleFormControlInput1" placeholder="">
        </div>
        <div class="mb-3">
            <label for="formFile" class="form-label">Image of product</label>
            <input class="form-control" name="image[]" type="file" id="formFile">
        </div>
        <div class="col-12">
            <button class="btn btn-primary" name="submit" value="Upload" type="submit">Add</button>
        </div>
      </form>

    </div>
</body>
</html>