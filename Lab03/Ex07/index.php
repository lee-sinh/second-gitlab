<?php
    $data = [
        [
            "model" => "msi",
            "name"  => "MSI Titan 18 HX A14VI",
            "core" => "i9 14900HX",
            "RAM" => 128,
            "SSD" => 2024,
            "original_price" => 5699,
            "discount_price" => 5699,
            "image" => "https://toptechkh.com/wp-content/uploads/2023/12/1-46.jpg",
        ],
        [
            "model" => "Acer",
            "name"  => "Acer Aspire 5 A515-58GM",
            "core" => "i7 1355U",
            "RAM" => 8,
            "SSD" => 512,
            "original_price" => 699,
            "discount_price" => 649,
            "image" => "https://static-ecpa.acer.com/media/catalog/product/a/c/acer-aspire-5-a515-58gm-with-fingerprint-steel-gray-01-1000x1000_nx.kgzaa.001.png?optimize=high&bg-color=255,255,255&fit=bounds&height=500&width=500&canvas=500:500&format=jpeg",
        ],
        [
            "model" => "Asus",
            "name"  => "Asus Vivobook X415MA-EK76",
            "core" => "Celeron N4020",
            "RAM" => 4,
            "SSD" => 512,
            "original_price" => 289,
            "discount_price" => 359,
            "image" => "https://toptechkh.com/wp-content/uploads/2023/12/1-1.jpg",
        ],
        [
            "model" => "mac",
            "name"  => "MacBook Pro 16",
            "core" => "i7 q1355U",
            "RAM" => 8,
            "SSD" => 512,
            "original_price" => 3699,
            "discount_price" => 3699,
            "image" => "https://nsphoneshop.com/wp-content/uploads/2022/04/macbook-pro-16-m1-pro-late-2021-space-gray.jpg",
        ],
        [
            "model" => "mac",
            "name"  => "MacBook Pro 16",
            "core" => "M3",
            "RAM" => 18,
            "SSD" => 1000,
            "original_price" => 3029,
            "discount_price" => 3099,
            "image" => "https://store.ee.co.uk/images/product/uni2/DigitalContent/600x450/hh/HH7L_49EFDEBA-3B06-4AA6-94C1-2C5C745D3688_large.jpg",
        ],
        [
            "model" => "lenovo",
            "name"  => "Lenovo ThinkPad E16 G1",
            "core" => "i5 1335U",
            "RAM" => 16,
            "SSD" => 512,
            "original_price" => 799,
            "discount_price" => 799,
            "image" => "https://www.campuspoint.de/media/catalog/product/cache/eb2124e8615c08c4ebf6eda39a88bc52/l/e/lenovo_thinkpad_e16_g1_amd_1b.jpg",
        ],
        [
            "model" => "Acer",
            "name"  => "Acer Aspire 5 A515-58GM",
            "core" => "i5 1335U",
            "RAM" => 8,
            "SSD" => 512,
            "original_price" => 599,
            "discount_price" => 549,
            "image" => "https://static-ecapac.acer.com/media/catalog/product/1/_/1_55_nx.khjsg.001_1_1.jpg?optimize=high&bg-color=255,255,255&fit=bounds&height=500&width=500&canvas=500:500",
        ],
        [
            "model" => "mac",
            "name"  => "MacBook Pro 16",
            "core" => "M3 Pro",
            "RAM" => 18,
            "SSD" => 512,
            "original_price" => 2669,
            "discount_price" => 2699,
            "image" => "https://i-shop.ru/upload/iblock/8b2/2o1ep4w9mtwdpblwzx39mtcf0153t02t.jpg",
        ],
        [
            "model" => "mac",
            "name"  => "MacBook Pro 14",
            "core" => "M3 Pro",
            "RAM" => 18,
            "SSD" => 1000,
            "original_price" => 3399,
            "discount_price" => 3399,
            "image" => "https://media.croma.com/image/upload/v1663348542/Croma%20Assets/Computers%20Peripherals/Laptop/Images/245233_0_l5bk1y.png",
        ],
        [
            "model" => "mac",
            "name"  => "MacBook Pro 14",
            "core" => "M3 Max",
            "RAM" => 36,
            "SSD" => 1000,
            "original_price" => 2599,
            "discount_price" => 2599,
            "image" => "https://cdn.shopify.com/s/files/1/0306/8677/products/apple-macbook-pro-14-inch-macbook-pro-14-inch-m1-pro-8-core-space-grey-2021-good-40236216647996.jpg",
        ],
    ];
    $logo = [
        "msi" => "https://logos-world.net/wp-content/uploads/2020/11/MSI-Logo.png",
        "mac"  => "https://cdn-icons-png.flaticon.com/512/2/2235.png",
        "asus" => "https://cdn-images-1.medium.com/max/2000/1*NwfoiV9f96_MhpmJwdinPA.png",
        "acer" => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2oBcygKUBpmyQMzMa0zLXotepwqQ2t-UUiYJeicIeRQ&s",
        "lenovo" => "https://1000logos.net/wp-content/uploads/2017/03/Lenovo-Logo-1.png",
    ];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Computer Store</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .col-sm-2{
            border: solid #BEBEBE 0.8px; 
            margin: 10px;
            padding: 10px;
            height: 350px;
        
        }
        img{
            object-fit: contain;
            margin: 5px;
        }
        .small-ratings i{
            color:#cecece;   
        }
    </style>
</head>
<body> 
 
    <div class="container-fluid bg-light">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">

                    <img src="https://media.istockphoto.com/id/1169455737/vector/computer-service-and-repair-logo-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=5WzcYez840S9OATFgP7RbOMOQQQeICjK48LGuZO11dc=" alt="Shop Logo" width="200px" height="100px">
                </div>
                <div class="col-sm-8">
       
                    <h2>Lee Computer</h2>
                </div>
                <div class="col-sm-2">

                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <?php
            $count = 0;
            foreach ($data as $product) {

                if ($count % 5 == 0) {
                    echo '<div class="row">';
                }

                echo '<div class="col-sm-2">';
                $model = strtolower($product["model"]); 
                if (array_key_exists($model, $logo)) {
                    echo "<img src='" . $logo[$model] . "' alt='Product Logo' width='100px' height='60px'>";
                }
                echo "<img src='" . $product["image"] . "' alt='Product Image' width='150px' height='100px'>";

                echo'<br>';
                echo'<span class="fa fa-star"></span>';
                echo'<span class="fa fa-star"></span>';
                echo'<span class="fa fa-star"></span>';
                echo'<span class="fa fa-star"></span>';
                echo'<span class="fa fa-star"></span>';

                echo '<h5 class="card-title">' . $product["name"] .'</h5>';
                echo '<h5 class="card-title">' . " ( ". $product["core"] ." / ". $product["RAM"]."Gb / ". $product["SSD"] ."Gb".' )</h5>';
                if ($product["original_price"] == $product["discount_price"]){
                    echo '<p class="card-text">$' . number_format($product["original_price"], 2) . '</p>';
                }
                    else{
                    echo '<p class="card-text">$' . number_format($product["discount_price"], 2) . '</p>';
                    echo '<p class="card-text"><s>$' . number_format($product["original_price"], 2) . '</s></p>';
                };
                echo '</div>';

                if ($count % 5 == 4 || $count == count($data) - 1) {
                    echo '</div>';
                }

                $count++;
            }
        ?>
    </div>


    <div class="container-fluid bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
 
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-instagram"></a>

                </div>
                <div class="col-sm-6">

                    <p>123 Main Street, City, Country</p>
                    <p>Phone: +123 456 7890</p>
                </div>
            </div>
        </div>
    </div>
</body> 
</html>
