<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS v5.2.1 -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
      crossorigin="anonymous"
    />
  </head>

  <body>
    <div class="container">
      <form action="#" method="POST" role="form" enctype="multipart/form-data">
        <legend>Upload images</legend>

        <div class="form-group">
          <label for="">Product name: </label>
          <input type="text" class="form-control" name="productname" placeholder="Enter name of product">
        </div>
        <div class="form-groupe">
          <label for="">Product</label>
          <input type="file" class="form-control" name="images[]" multiple>
        </div>

        <input type="submit" class="btn btn-success" value="Upload" name="submit">
      </form>
    </div>

    <div class="container">
      <?php
        if(isset($_POST['submit'])) {
          $files = $_FILES['images'];
          var_dump($files);
          $i = 0;

          foreach ($files['name'] as $name) {
            $tmp_name = $files['tmp_name'][$i];

            move_uploaded_file($tmp_name, 'img_uploaded/'.$name);
            echo "<img src='img_uploaded/$name' height=50>";
            $i++;
          }
        }
      ?>
    </div>
    <!-- Bootstrap JavaScript Libraries -->
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
      integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r"
      crossorigin="anonymous"
    ></script>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"
      integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+"
      crossorigin="anonymous"
    ></script>
  </body>
</html>