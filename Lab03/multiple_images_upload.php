<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>

<div class="container">

    <form action="" method="POST" role="form" enctype="multipart/form-data">
        <legend>Upload images</legend>
    <div class="form-group">
        <label for="">Product name: </label>
        <input type="text" class="form-control" name="productname"
        placeholder="Enter name of product">
    </div>

    <div class="form-group">
        <label for="">Product images: </label>
        <input type="file" class="form-control" name="images[]" multiple>
    </div>

    <input type="submit" class="btn btn-success" value="Upload" name="submit">

    </form>
</div>

<div class="container">
    <?php
        if(isset($_POST['submit'])){
        $files = $_FILES['images'];
        var_dump($files);
        }
    ?>
</div>
</body>
</html>