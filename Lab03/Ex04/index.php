<?php
$targetDirectory = 'uploadedFiles/';

if (isset($_POST['submit'])) {
    $description = $_POST['description'];
    $file = $_FILES['file'];

    if (strlen($description) < 5) {
        die("Error: Text description must be at least 5 characters.");
    }

    $allowedFileTypes = ['png', 'pdf'];
    $fileExtension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

    if (!in_array($fileExtension, $allowedFileTypes)) {
        die("Error: Only PNG and PDF files are allowed.");
    }

    $maxFileSize = 10 * 1024 * 1024;
    if ($file['size'] > $maxFileSize) {
        die("Error: File size exceeds the maximum limit (10MB).");
    }

    $uniqueFilename = uniqid() . '_' . $file['name'];

    $targetPath = $targetDirectory . $uniqueFilename;
    if (move_uploaded_file($file['tmp_name'], $targetPath)) {
        echo "File uploaded successfully. Description: $description";
    } else {
        echo "Error: Unable to move the file.";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>File Upload Form</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="upload.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="description">Text Description (at least 5 characters):</label>
                    <input type="text" class="form-control" id="description" name="description" required minlength="5">
                </div>

                <div class="form-group">
                    <label for="file">Choose a file (png or pdf, max 10MB):</label>
                    <input type="file" class="form-control-file" id="file" name="file" accept=".png, .pdf" required>
                </div>

                <button type="submit" class="btn btn-primary" name="submit">Upload File</button>
            </form>
        </div>
    </div>
</div>

<!-- Add Bootstrap JS and Popper.js scripts -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>
