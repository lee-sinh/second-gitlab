
<?php
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST["submit"])){
        $text = $_POST["txt"];
        $image = $_POST["img"];

        // Check if the image URL is not empty and ends with .png or .jpg
        if (!empty($product["image"]) && (substr($product["image"], -4) == ".png" || substr($product["image"], -4) == ".jpg")) {
            echo "<td><img src='" . $product["image"] . "' height='40px'></td>";
        } else {
            echo "<td>No Image</td>";
        }
        
        echo "<p>".$text."</p>";
        }
    }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">
        <form class="was-validated" method="POST">
            <div class="mb-3">
                <label for="validationTextarea" class="form-label">Textarea, must be 8-20 characters long.</label>
                <textarea class="form-control" name="txt" id="validationTextarea" placeholder="Required example textarea" required></textarea>
                <div class="invalid-feedback">
                Please enter a message in the textarea.
                </div>
            </div>

            <div class="mb-3">
                <input type="file" class="form-control" name="image" aria-label="file example" required>
                <div class="invalid-feedback">Example invalid form file feedback</div>
            </div>

            <div class="mb-3">
                <button class="btn btn-primary" name="submit" type="submit">Submit form</button>
            </div>
        </form>
    </div>
</body>
</html>
