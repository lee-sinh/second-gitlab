<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST["submit"])) {
                $txt = $_POST["txt"];
                $pswd = $_POST["pswd"];
                $radio = $_POST["radio"];
                $check = $_POST["remember"];

                echo "<h3>All information:</h3>";
                echo "<p>Name: $txt</p>";
                echo "<p>Password: $pswd</p>";
                echo "<p>Toggle: $radio</p>";
                echo "<p>Check: $check</p>";

            }
        }
    ?>

    <div class="container">
        <form action="" class="needs-validation" method="POST">
            <div class="mb-3 mt-3">
                <label for="uname" class="form-label">Text:</label>
                <input type="text" class="form-control" id="uname" placeholder="Enter any text" name="txt" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>
            <div class="mb-3">
                <label for="pwd" class="form-label">Password:</label>
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>
            <div class="form-check mb-3">
                <input type="radio" class="form-check-input" id="validationFormCheck3" name="radio" required>
                <label class="form-check-label" for="validationFormCheck3">Toggle this radio</label>
                <div class="invalid-feedback">Check this radio to continue.</div>
            </div>
            <div class="form-check mb-3">
                <input class="form-check-input" type="checkbox" id="myCheck" name="remember" required>
                <label class="form-check-label" for="myCheck">I agree for the privacy.</label>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Check this checkbox to continue.</div>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

</body>
</html>