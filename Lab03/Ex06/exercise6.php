<?php
$users = [
    [
        "name" => "Lindsay Walton",
        "title" => "Front-end Developer",
        "email" => "lindsay.walton@example.com",
        "role" => "Member"
    ],
    [
        "name" => "Courtney Henry",
        "title" => "Designer",
        "email" => "courtney.henry@example.com",
        "role" => "Admin"
    ],
    [
        "name" => "Tom Cook",
        "title" => "Director of Product",
        "email" => "tom.cook@example.com",
        "role" => "Member"
    ],
    [
        "name" => "Whitney Francis",
        "title" => "Copywriter",
        "email" => "whitney.francis@example.com",
        "role" => "Admin"
    ],
    [
        "name" => "Leonard Krasner",
        "title" => "Senior Designer",
        "email" => "leonard.krasner@example.com",
        "role" => "Owner"
    ],
    [
        "name" => "Floyd Miles",
        "title" => "Principal Designer",
        "email" => "floyd.miles@example.com",
        "role" => "Member"
    ]
];

function displayUsers($users)
{
    echo '<div class="container mt-4">
            <h2>Users</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>';

    foreach ($users as $user) {
        echo '<tr>
                <td>' . $user['name'] . '</td>
                <td>' . $user['title'] . '</td>
                <td>' . $user['email'] . '</td>
                <td>' . $user['role'] . '</td>
                <td>
                    <form action="edit_user.php" method="post" class="d-inline">
                        <input type="hidden" name="name" value="' . $user['name'] . '">
                        <input type="hidden" name="title" value="' . $user['title'] . '">
                        <input type="hidden" name="email" value="' . $user['email'] . '">
                        <input type="hidden" name="role" value="' . $user['role'] . '">
                        <button type="submit" class="btn btn-primary btn-sm">Edit</button>
                    </form>
                </td>
            </tr>';
    }

    echo '</tbody></table></div>';
}

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["submit"])) {
    $newUser = [
        "name" => $_POST["name"],
        "title" => $_POST["title"],
        "email" => $_POST["email"],
        "role" => $_POST["role"]
    ];

    $users[] = $newUser;
}

displayUsers($users);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Management</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-4">
        <h2>Add User</h2>
        <form action="" method="post">
            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" name="name" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="title" class="form-label">Title:</label>
                <input type="text" name="title" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email:</label>
                <input type="email" name="email" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="role" class="form-label">Role:</label>
                <select name="role" class="form-select" required>
                    <option value="Member">Member</option>
                    <option value="Admin">Admin</option>
                    <option value="Owner">Owner</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success" name="submit">Add User</button>
        </form>
    </div>

    <!-- Bootstrap JS and Popper.js (required for Bootstrap components) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
