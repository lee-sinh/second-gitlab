<?php 
$staffs = [
  [
    "profile_image" => "https://static.vecteezy.com/system/resources/previews/020/389/525/original/hand-drawing-cartoon-girl-cute-girl-drawing-for-profile-picture-vector.jpg",
    "name" => "Jane Cooper",
    "role" => "Paradigm Representative",
    "position" => "admin"
  ],
  [
    "profile_image" => "https://i.etsystatic.com/15418561/r/il/f06c80/3233862560/il_fullxfull.3233862560_jwqd.jpg",
    "name" => "Cody Fisher",
    "role" => "Lead security Associate",
    "position" => "admin"
  ],
  [
    "profile_image" => "https://i.pinimg.com/736x/70/aa/28/70aa28f678193194b4a023e542ce4775.jpg",
    "name" => "Esther Howard",
    "role" => "Assurance Administrator",
    "position" => "admin"
  ],
  [
    "profile_image" => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu3wxsIL1penECU-Rai-gPm2kHpBABIL7cef5Btcv2RcsOU3Tu1p2Lx5Pop1LW6YcYggo&usqp=CAU",
    "name" => "Kristin Watson",
    "role" => "Investor Data Orchestrator",
    "position" => "admin"
  ],
  [
    "profile_image" => "https://cdn5.vectorstock.com/i/1000x1000/00/69/man-profile-cartoon-vector-19490069.jpg",
    "name" => "Cameron Williamson",
    "role" => "Product Infrastructure Executive",
    "position" => "admin"
  ],
  [
    "profile_image" => "https://i.pinimg.com/736x/23/ce/29/23ce298e6a33476fa0c197f9cdf2046b.jpg",
    "name" => "Courtney Henry",
    "role" => "Investor Factors Associate",
    "position" => "admin"
  ],
];

function addStaff(&$staffs) {
    if(isset($_POST["submit"])) {
      $name = $_POST["name"];
      $role =  $_POST["role"];
      $position = $_POST["position"];
      $file = $_FILES['image'];
      var_dump($file);         

      $tmp_name = $file['tmp_name'];
      move_uploaded_file($tmp_name, 'profileUplaoded/'.$file['name']);

      $file_path = 'profileUplaoded/'.$file['name'];
      
      $newStaff = array(
        "profile_image" => $file_path,
        "name" => "$name",
        "role" => "$role",
        "position" => "$position"
      );
      array_push($staffs, $newStaff);
      var_dump($staffs);
    }

}

function renderProfile($staffs) {
  echo'<div class="container d-flex flex-wrap justify-content-center">';
  for($i = 0; $i < count($staffs); $i++) {
    echo '
        <div class="p-2">
          <div class="card text-start" style="width: 25rem;">
            <div class="d-flex justify-content-center">
              <img class="card-img-top rounded-circle p-3" src="'.$staffs[$i]["profile_image"].'" alt="Title" style="width: 15rem;"/>
            </div>
            <div class="card-body text-center pt-0 mb-3">
              <h4 class="card-title">'.$staffs[$i]["name"].'</h4>
              <p class="card-text">'.$staffs[$i]["role"].'</p>
              <span class="badge rounded-pill text-bg-success">'.$staffs[$i]['position'].'</span>
            </div>
            <div class="btn-toolbar " role="toolbar" aria-label="Toolbar with button groups">
              <div class="btn-group w-100 " role="group" aria-label="First group">
                <button type="button" class="btn btn-outline-secondary border-bottom-0 border-start-0 rounded-0"><i class="fa-solid fa-envelope me-3"></i>Email</button>
                <button type="button" class="btn btn-outline-secondary border-bottom-0 border-start-0 border-end-0 rounded-0"><i class="fa-solid fa-phone me-3"></i>Call</button>
              </div>
            </div>
          </div>
        </div>
        ';
      }
  echo '</div>';
}

?>
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS v5.2.1 -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
      crossorigin="anonymous"
    />
    <script src="https://kit.fontawesome.com/b0208632c0.js" crossorigin="anonymous"></script>
  </head>

  <body>
    <main>

      <?php
        addStaff($staffs);
        // var_dump($staffs);
        renderProfile($staffs);
        // var_dump($staffs);
      ?>
    </main>
    <!-- Bootstrap JavaScript Libraries -->
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
      integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r"
      crossorigin="anonymous"
    ></script>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"
      integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+"
      crossorigin="anonymous"
    ></script>
  </body>
</html>

