<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="delivery.css">
    <title>Document</title>
</head>
<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["submit"])) {
            $name = $_POST["name"];
            $email = $_POST["email"];
            $password = $_POST["password"];
            $confirmPassword = $_POST["confirmPassword"];

            echo "<h3>Registration Data:</h3>";
            echo "<p>Name: $name</p>";
            echo "<p>Email: $email</p>";
            echo "<p>Password: $password</p>";
            echo "<p>Confirm Password: $confirmPassword</p>";
        }
    }
    ?>

    <div class="container">
        <div class="box1">
            <div class="card">
                <div class="card-body">ABCD Delivery Express</div>
            </div>
            <br>
            <p style="text-align: center; font-size: larger;">ABCD Delivery Express is continuously looking for delivery and <br>
                pickup agents all over Phnom Penh city. Why not join our <br>
                network and make some extra pocket money!</p>
            <img src="https://news.sophos.com/wp-content/uploads/2021/07/hd-1200-2.png" class="img-fluid" alt="...">

            <br><br>
            <div class="panel panel-default">
                  <div class="panel-footer" style="text-align: center;">
                        About 
                        FAQ
                        Contact
                    </div>
            </div>
            
        </div>
        <div class="box2">
            <h2 style="text-align: center; margin-top: -560px;">Register as a Delivery <br>Agent</h2>
            <p style="text-align: center;"> To register, please fill in the following details. We <br> will then contact you shortly.</p>
            
            <form class="input" action="#" method="POST">
                <input type="text" name="name" placeholder="Name or nickname" required>
                <input type="email" name="email" placeholder="E-mail Address" required>
                <input type="password" name="password" placeholder="Password" required>
                <input type="password" name="confirmPassword" placeholder="Confirm Password" required>
                <button type="submit" name="submit" class="btn">Create My Account</button>
                <p class="login">Already have an account?<a class="signup" href="#">Log in</a></p>
            </form>
        </div>
    </div>
</body>
</html>