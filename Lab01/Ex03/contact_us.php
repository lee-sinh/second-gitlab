<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="contact_us.css">

    <!-- link -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
</head>
<body>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST["submit"])) {
                $username = $_POST["username"];
                $emailAdd= = $_POST["emailAdd"];
                $message = $_POST["message"];

                echo '<h1> Welcome ' .$username.' ! </h1><br>';
                echo '<p>Email:' .$emailAdd. '</p>';
                echo '<p>Your message is:' .$message.'</p>';
            }
            // else (isset($_POST["submit"])) {
            //     $email = $_POST["email"];
            //     $password = $_POST["password"];

            //     echo '<h2>Your Email Address is:' .$email.'</h2>';
            //     echo '<ph3>Password:' .$password. '</ph3>';
            // }
        }
    ?>

    <div class="box">
        <div class="Contact-Us-box">
            <div class="table">
                <h1><b>Contact Us</b></h1>
                <form action="" method="POST">

                    <div class="data">
                        <label style="width: 109px; text-align: center;">Your Name</label>
                        <input type="text" name="username"  required>
                    </div>
                                 
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa-solid fa-user fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <div class="email">
                        <label style=" text-align: center;">Email Address</label>
                        <input type="text" name="emailAdd" required>
                    </div>
                                 
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa fa-envelope fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <div class="textarea">
                        <label style=" text-align: center;">Your Message</label>
                        <textarea rows="4" cols="50" form="usrform" ></textarea>
                    </div>
                    <div class="button">
                        <button type="submit" name="submit">Send Message</button> 
                    </div>

                </form>
            </div>
        </div>

        <div class="Login-Account-box">
            <div class="table">
                <h1><b>Login Account</b></h1>
                <form action="" method="POST">

                    <div class="data">
                        <label style="margin-top: 30px; text-align: center;" >Email Address</label>
                        <input type="text" required>
                    </div>
                                 
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa fa-envelope fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <div class="email">
                        <label style="margin-top: 30px; width: 105px; text-align: center;" >Password</label>
                        <input type="text"  required>
                    </div>
                                 
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa-solid fa-shield fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <label class="remember">
                        <input type="checkbox" checked="checked" name="remember"> Save Password
                    </label>

                    <div class="forgot">
                        <a href="#" style="color: black;">Forgot Password?</a>
                    </div>

                    <div class="button">
                        <button style="margin-top: 50px;" type="submit">Login Account</button> 
                    </div>

                    <div class="question">
                        <span>Don't have an account ?<a href="#" class="Login"> SIGN UP</a></span>
                    </div>

                </form>
            </div>
        </div>
        
        <div class="Create-Account-box">
            <div class="table">
                <h1><b>Create Account</b></h1>
                <form action="/cute.php" method="post">

                    <div class="data">
                        <label style="margin-top: 15px; width: 105px; text-align: center;" >Full name</label>
                        <input type="text"  required>
                    </div>     
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa-solid fa-user fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <div class="email">
                        <label style="margin-top: 15px; text-align: center;" >Email Address</label>
                        <input type="text"  required>
                    </div>     
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa fa-envelope fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    
                    <div class="email">
                        <label style="margin-top: 15px; text-align: center;" >Phone Number</label>
                        <input type="text"  required>
                    </div>     
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa-solid fa-phone fa-1x" style="color: blue;"></i>
                        </div>
                    </div>

                    <div class="email">
                        <label style="margin-top: 15px; text-align: center; width: 105px;" >Password</label>
                        <input type="text"  required>
                    </div>     
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fa-solid fa-shield fa-1x" style="color: blue;"></i>
                        </div>
                    </div>
                    
                    <div class="email">
                        <label style="margin-top: 15px; text-align: center; width: 200px;" > Comfirm Password</label>
                        <input type="text"  required>
                    </div>     
                    <div class="social-icons">
                        <div class="circle-container">
                            <i class="fas fa-check-circle fa-1x" style="color: blue;"></i>
                        </div>
                    </div>
                    
                    
                    <label class="remember">
                        <input type="checkbox" checked="checked" name="remember"> Agree with <b>Term & Conditions</b>
                    </label>

                    <div class="button">
                        <button style="margin-top: 20px;" type="submit">CreateAccount</button> 
                    </div>

                    <div class="question" style="margin-top: 30px;">
                        <span>Already have an account ?<a href="#" class="Login"> Login</a></span>
                    </div>

                </form>
            </div>
        </div>

    </div>

</body>
</html>