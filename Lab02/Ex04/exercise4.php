<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Ex04</title>
</head>
<body>
    <?php
        $cars = [
            ["name" => "Volvo", "stock" => 22, "sold" => 18], 
            ["name" => "BMW", "stock" => 15, "sold" => 13], 
            ["name" => "Saab", "stock" => 5, "sold" => 2], 
            ["name" => "Land Rover", "stock" => 17, "sold" => 15]
        ];
    ?>
<div class="container mt-3">
  <h2>Bootstrap table</h2>        
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Stock</th>
        <th>Sold</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($cars as $car){
            echo "<tr>";
                echo "<td>".$car['name']."</td>";
                echo "<td>".$car['stock']."</td>";
                echo "<td>".$car['sold']."</td>";
            echo "</tr>";
        }
        ?>

    </tbody>
  </table>
</div>

</body>
</html>