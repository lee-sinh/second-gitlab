<?php
    $rand_num = [];
    echo "Random numbers: ";
    for($i=0; $i < 50; $i++){
        $rand_num[] = rand(1, 100);
        echo $rand_num[$i].", ";
    }
    echo "</p>";
    echo "<hr>";

    sort($rand_num);
    echo "Sorted random numbers using ascending order: ";
    foreach($rand_num as $r){
        echo $r.", ";
    }
    echo "</p>";
    echo "<hr>";

    //add number 2024 to array
    array_push($rand_num, "2024");
    //sort array using descending order
    arsort($rand_num);
    echo "Sorted random numbers using descending order: ";
    foreach($rand_num as $r){
        echo $r.", ";
    }
?>
