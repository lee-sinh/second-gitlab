<?php
    $fruits = [
        "orange", "banana", "dragon fruit", "apple", "longan"
    ];

    echo "<p>$fruits[1]</p>";

    foreach ($fruits as $f) {
        echo "<ul>
            <li><p>$f</p></li>
        </ul>";
    }

    for($f=0; $f<count($fruits); $f++){
        echo $fruits[$f]. " ";
    }
    
    echo "<br>";

    array_push($fruits, "grape");
    unset($fruits[0]);
    foreach ($fruits as $f) {
        echo "<ul>
            <li><p>$f</p></li>
        </ul>";
    }
?>