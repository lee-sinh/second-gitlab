<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Ex03</title>
</head>
<body>
    <?php
    $students = [
        ["id" => "01", "first_name" => "Samnang", "last_name" => "Kong", "gender" => "Male", "age" => 18],
        ["id" => "02", "first_name" => "Piseth", "last_name" => "Mean", "gender" => "Male", "age" => 19],
        ["id" => "03", "first_name" => "Sreanghour", "last_name" => "Hy", "gender" => "Male", "age" => 18],
        ["id" => "04", "first_name" => "Sokhom", "last_name" => "Kim", "gender" => "Male", "age" => 18]
    ];
    ?>

    <div class="container mt-3">
    <h2>Bootstrap Table</h2>
    
    <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
            <tr>
                <th name="">ID</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Gender</th>
                <th>Age</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($students as $student){
                echo "<tr>";
                echo "<td>".$student['id']."</td>";
                echo "<td>".$student['first_name']."</td>";
                echo "<td>".$student['last_name']."</td>";
                echo "<td>".$student['gender']."</td>";
                echo "<td>".$student['age']."</td>";
                echo "</tr>";                
            }
            ?>

        </tbody>
        </table>
    </div>
    </div>

</body>
</html>
