<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
    <?php
        $data = [
            "fruit" => [
                "apple" => "ផ្លែប៉ោម",
                "banana" => "ផ្លែចេក",  
                "orange" => "ក្រូច",
                "grape" => "ផ្លែទំពាំងបាយជូរ"
            ],
            "occupation" => [
                "teacher" => "គ្រូបង្រៀន",
                "doctor" => "វេជ្ជបណ្ឌិត",
                "student" => "សិស្ស",
                "Mobile app developer" => "អ្នកអភិវឌ្ឍន៍កម្មវិធីទូរស័ព្ទ",
                "Web developer" => "អ្នកអភិវឌ្ឍន៍កម្មវិធីវែប",
                "Data scientist" => "អ្នកវិទ្យាសាស្ត្រទិន្នន័យ"
            ],
            "courses" => [
                "Math" => "?",
                "English" => "?",
                "Computer" => "?",
                "Physics" => "?",
                "Biology" => "?",
                "Chemistry" => "?"
            ]
        ];        
    ?>
<div class="container mt-3">
  <h2>Dictionary (En to Kh)</h2>         
  <table class="table table-bordered">

    <?php foreach($data as $key => $value){
        echo "<thead>";
            echo "<tr>";
                echo "<th>".$key."</th>";
            echo "</tr>";
        echo "</thead>";
        foreach($value as $k => $v){
        echo "<tbody>";
            echo "<tr>";
                echo "<td>$k : $v</td>";
            echo "</tr>";   
        echo "</tbody>";
        }
    }
    ?>

  </table>
</div>

</body>
</html>