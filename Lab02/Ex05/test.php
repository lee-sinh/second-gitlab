<?php

$data = [
    [
        "logo" => "https://logos-world.net/wp-content/uploads/2020/11/MSI-Logo.png",
        "model" => "MSI",
        "name" => "Titan 18 HX A14VI",
        "core" => "I9 14900HX",
        "RAM" => 128,
        "SSD" => 512,
        "original_price" => 5699,
        "profile" => "https://www.hidevolution.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/s/msi_nb_titan_18_a14v_photo02.png",
        "rating" => 5,
    ],
    [
        "logo" => "https://1000logos.net/wp-content/uploads/2016/09/Acer-Logo-500x313.png",
        "model" => "Acer",
        "name" => "Aspire 5 A515-58GM",
        "core" => "i7 1355U",
        "RAM" => 8,
        "SSD" => 512,
        "original_price" => 699,
        "discount_price" => 649,
        "profile" => "https://static-ecpa.acer.com/media/catalog/product/a/c/acer-aspire-5-a515-58m-a515-58mt-s50-55-with-fingerprint-with-backlit-on-wallpaper-start-screen-steel-gray-01-1000x1000_nx.khgaa.001.png?optimize=high&bg-color=255,255,255&fit=bounds&height=500&width=500&canvas=500:500&format=jpeg",
        "rating" => 4.5,
    ],
    [
        "logo" => "https://logowik.com/content/uploads/images/424_asus.jpg",
        "model" => "Asus",
        "name" => "Vivobook X415MA-EK7693W",
        "core" => "Celeron N4020",
        "RAM" => 4,
        "SSD" => 512,
        "original_price" => 359,
        "discount_price" => 289,
        "profile" => "https://dlcdnwebimgs.asus.com/gain/121675fb-0f2d-4287-81fb-e36363a145d9/",
        "rating" => 4.0,
    ],
    [
        "logo" => "https://media.designrush.com/inspiration_images/134802/conversions/_1511456315_653_apple-mobile.jpg",
        "model" => "MacBook",
        "name" => "Pro 16",
        "core" => "M3 Max",
        "RAM" => 36,
        "SSD" => 1,
        "original_price" => 3699,
        "profile" => "https://media.croma.com/image/upload/v1698840268/Croma%20Assets/Computers%20Peripherals/Laptop/Images/301935_7_ui4wm4.png",
        "rating" => 4.0,
    ],
    [
        "logo" => "https://media.designrush.com/inspiration_images/134802/conversions/_1511456315_653_apple-mobile.jpg",
        "model" => "MacBook",
        "name" => "Pro 16",
        "core" => "M3 Max",
        "RAM" => 36,
        "SSD" => 512,
        "original_price" => 3099,
        "discount_price" => 3029,
        "profile" => "https://media.croma.com/image/upload/v1698840268/Croma%20Assets/Computers%20Peripherals/Laptop/Images/301935_7_ui4wm4.png",
        "rating" => 4.0,
    ],
    [
        "logo" => "https://1000logos.net/wp-content/uploads/2017/03/Lenovo-Logo-1.png",
        "model" => "Lenovo",
        "name" => "ThinkPad E16 G1",
        "core" => "i5 1335U",
        "RAM" => 16,
        "SSD" => 512,
        "original_price" => 799,
        "profile" => "https://fivetech.co.uk/pub/media/catalog/product/cache/bf0b49dff1f2c883e632dd01c95205c0/a/d/ad6749563182160381860fe48a90ac21.jpg",
        "rating" => 4.8,
    ],
    [
        "logo" => "https://1000logos.net/wp-content/uploads/2016/09/Acer-Logo-500x313.png",
        "model" => "Acer",
        "name" => "Aspire 5 A515-58GM",
        "core" => "i7 1355U",
        "RAM" => 8,
        "SSD" => 512,
        "original_price" => 599,
        "discount_price" => 549,
        "profile" => "https://static-ecpa.acer.com/media/catalog/product/a/c/acer-aspire-5-a515-58m-a515-58mt-s50-55-with-fingerprint-with-backlit-on-wallpaper-start-screen-steel-gray-01-1000x1000_nx.khgaa.001.png?optimize=high&bg-color=255,255,255&fit=bounds&height=500&width=500&canvas=500:500&format=jpeg",
        "rating" => 4.5,
    ],
    [
        "logo" => "https://media.designrush.com/inspiration_images/134802/conversions/_1511456315_653_apple-mobile.jpg",
        "model" => "MacBook",
        "name" => "Pro 16",
        "core" => "M3 Pro",
        "RAM" => 18,
        "SSD" => 512,
        "original_price" => 2699,
        "discount_price" => 2669,
        "profile" => "https://media.croma.com/image/upload/v1698840268/Croma%20Assets/Computers%20Peripherals/Laptop/Images/301935_7_ui4wm4.png",
        "rating" => 4.8,
    ],
    [
        "logo" => "https://media.designrush.com/inspiration_images/134802/conversions/_1511456315_653_apple-mobile.jpg",
        "model" => "MacBook",
        "name" => "Pro 16",
        "core" => "M3 Max",
        "RAM" => 36,
        "SSD" => 1,
        "original_price" => 3399,
        "profile" => "https://media.croma.com/image/upload/v1698840268/Croma%20Assets/Computers%20Peripherals/Laptop/Images/301935_7_ui4wm4.png",
        "rating" => 4.0,
    ],
    [
        "logo" => "https://media.designrush.com/inspiration_images/134802/conversions/_1511456315_653_apple-mobile.jpg",
        "model" => "MacBook",
        "name" => "Pro 14",
        "core" => "M3 Pro",
        "RAM" => 18,
        "SSD" => 1,
        "original_price" => 2599,
        "profile" => "https://media.croma.com/image/upload/v1698840268/Croma%20Assets/Computers%20Peripherals/Laptop/Images/301935_7_ui4wm4.png",
        "rating" => 4.0,
    ],
];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laptop Products</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .products-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            gap: 20px;
            margin-left: 30px; /* Adjusted left margin */
        }

        .product {
            position: relative;
            border: 1px solid #ccc;
            border-radius: 10px;
            padding: 10px;
            width: 300px;
            height: 500px;
            display: inline-block;
            display: table-cell;
        }

        .product img {
            max-width: 100%;
            height: auto;
        }

        .checked {
            color: orange;
        }

        .rating {
            margin-top: -25px;
        }

        .price {
            font-size: 20px;
        }

        .discount {
            color: gray;
        }
    </style>
</head>

<body>
    <div class="products-container">
        <?php foreach ($data as $product) : ?>
            <div class="product">
                <img src="<?= $product['logo']; ?>" alt="<?= $product['name']; ?> Logo" width="100px" height="100px">

                <?php if ($product['model'] === 'Acer') : ?>
                    <img class="acer-profile" src="<?= $product['profile']; ?>" alt="<?= $product['name']; ?>">
                <?php else : ?>
                    <img src="<?= $product['profile']; ?>" alt="<?= $product['name']; ?>">
                <?php endif; ?>

                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) : ?>
                        <i class="fa fa-star <?= $i <= $product['rating'] ? 'checked' : ''; ?>"></i>
                    <?php endfor; ?>
                </div>

                <p style="color: gray; font-size: 20px;"><?= sprintf("%s %s (%s / %dGB / SSD%d... ", $product['model'], $product['name'], $product['core'], $product['RAM'], $product['SSD']); ?></p>

                <?php if (isset($product['discount_price'])) : ?>
                    <p class="price">
                        <b>$<?= number_format($product['discount_price'], 2); ?></b> &emsp;
                        <del class="discount">$<?= number_format($product['original_price'], 2); ?></del>
                    </p>
                <?php else : ?>
                    <p class="price">
                        <b>$<?= number_format($product['original_price'], 2); ?></b>
                    </p>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</body>

</html>