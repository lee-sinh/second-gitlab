<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);


    $server = "localhost";
    $username = "root";
    $password = "root";
    $database = "dbtest";

    $con = new mysqli($server, $username, $password, $database, 8889);

    if($con->connect_error){
        echo "<h3>Connection to database error!</h3>";
    }
    else{
        echo "<h3>Connection to database successful!</h3>";
    }

?>