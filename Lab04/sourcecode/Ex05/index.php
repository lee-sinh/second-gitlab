<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="container">

    <h2>Product List</h2>
    
    <form action="search.php" method="GET">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Search by name or category" name="search">
            <button class="btn btn-outline-secondary" type="submit">Search</button>
        </div>
    </form>
    
    <table class="table">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Category</th>
            <th scope="col">Description</th>
            <th scope="col">Image</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'db-connection.php';
                echo "<button class='btn btn-primary' type='submit'>Add Product</button>";

                $sql = "SELECT  p.id, p.price, p.name, c.name AS category, p.description, i.picture_url
                        FROM product AS p
                        JOIN category AS c ON p.category_id = c.id
                        LEFT JOIN image AS i ON p.image_id = i.id";

                $data = $con->query($sql);

                //var_dump($data)
                if($data->num_rows > 0){
                    while($row = $data->fetch_assoc()){
                        $id = $row['id'];
                        $name = $row['name'];
                        $price = $row['price'];
                        $category = $row['category'];
                        $description = $row['description'];
                        $image = $row['picture_url'];

                    echo "<tr>
                        <td>$id</td>
                        <td>$name</td>
                        <td>$price$</td>
                        <td>$category</td>
                        <td>$description</td>
                        <td><img src='$image' height = 40px></td>
                        </tr>";
                    }
                }
            ?>
        </tbody>
    </table>
</div> 
</body>
</html>