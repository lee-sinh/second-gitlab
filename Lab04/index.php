
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Database</title>
</head>
<body>
<div class="container">
    <table class="table">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Author</th>
            <th scope="col">Type</th>
            <th scope="col">Year</th>
            <th scope="col">Picture</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'connection.php';

                $sql =  'SELECT * FROM Book';

                $data = $con->query($sql);

                //var_dump($data)
                if($data->num_rows > 0){
                    while($row = $data->fetch_assoc()){
                        $id = $row['id'];
                        $author = $row['author'];
                        $type = $row['type'];
                        $year = $row['year'];
                        $picture = $row['picture'];

                    echo "<tr>
                        <td>$id</td>
                        <td>$author</td>
                        <td>$type</td>
                        <td>$year</td>
                        <td><img src='$picture' height = 40px></td>
                        </tr>";
                    }
                }
            ?>
        </tbody>
    </table>
</div> 
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</html>