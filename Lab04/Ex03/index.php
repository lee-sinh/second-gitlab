<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="container">
    <table class="table">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'db-connection.php';

                $sql =  'SELECT * FROM product';

                $data = $con->query($sql);

                //var_dump($data)
                if($data->num_rows > 0){
                    while($row = $data->fetch_assoc()){
                        $id = $row['id'];
                        $name = $row['name'];
                        $price = $row['price'];
                        $description = $row['description'];

                    echo "<tr>
                        <td>$id</td>
                        <td>$name</td>
                        <td>$price$</td>
                        <td>$description</td>
                        </tr>";
                    }
                }
            ?>
        </tbody>
    </table>



    <div class='container mt-5'>

    <?php

        $sql2 = "SELECT p.price, p.name, c.name AS category, p.description FROM product AS p JOIN category AS c ON p.category_id = c.id";
        $data2 = $con->query($sql2);

        if($data2->num_rows > 0){
            while($row = $data2->fetch_assoc()){
            echo "<div class='card' style='width: 18rem; display: inline-block; margin: 10px;'>";
            echo "<div class='card-body'>";
            echo "<h5 class='card-title'>{$row['name']}</h5>";
            echo "<p class='card-text'>Price: {$row['price']}$</p>";
            echo "<p class='card-text'>Category: {$row['category']}</p>";
            echo "<p class='card-text'>Description: {$row['description']}</p>";
            echo "</div></div>";
            }
        }
    ?>
    </div>
</div> 
</body>
</html>